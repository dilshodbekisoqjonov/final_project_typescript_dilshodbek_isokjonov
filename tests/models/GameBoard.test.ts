import { GameBoard } from '../../src/models/GameBoard';
import { levelConfigs } from '../../src/utils/levels';

describe('GameBoard', () => {
  test('should initialize with the correct board size and mine count', () => {
    const { rows, cols, mineCount } = levelConfigs.easy;
    const board = new GameBoard(rows, cols, mineCount);

    expect(board.rows).toBe(rows);
    expect(board.cols).toBe(cols);
    expect(board.mineCount).toBe(mineCount);
  });

  test('should place mines correctly', () => {
    const { rows, cols, mineCount } = levelConfigs.easy;
    const board = new GameBoard(rows, cols, mineCount);
  
    let minesPlaced = 0;
    for (let row = 0; row < rows; row++) {
      for (let col = 0; col < cols; col++) {
        const cell = board.getCell(row, col);
        if (cell.isMine) {
          minesPlaced++;
        }
      }
    }
  
    expect(minesPlaced).toBe(mineCount);
  });
}); 