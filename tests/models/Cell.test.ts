import { Cell } from '../../src/models/Cell';

describe('Cell', () => {
  test('should initialize with the correct properties', () => {
    const cell = new Cell(true);

    expect(cell.isMine).toBe(true);
    expect(cell.isRevealed).toBe(false);
    expect(cell.isFlagged).toBe(false);
    expect(cell.adjacentMineCount).toBe(0);
  });

  test('should update properties correctly', () => {
    const cell = new Cell(false);
  
    cell.isRevealed = true;
    cell.isFlagged = true;
    cell.adjacentMineCount = 3;
  
    expect(cell.isRevealed).toBe(true);
    expect(cell.isFlagged).toBe(true);
    expect(cell.adjacentMineCount).toBe(3);
  });
}); 