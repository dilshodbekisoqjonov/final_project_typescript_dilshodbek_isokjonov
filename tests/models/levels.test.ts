// tests/utils/levels.test.ts
import { levelConfigs } from '../../src/utils/levels';

describe('levelConfigs', () => {
  test('should have the correct configurations for each difficulty level', () => {
    expect(levelConfigs.easy).toEqual({
      rows: 10,
      cols: 10,
      mineCount: 5,
    });

    expect(levelConfigs.medium).toEqual({
      rows: 20,
      cols: 20,
      mineCount: 10,
    });

    // Add more assertions for other difficulty levels if needed
  });
});