import { Game } from '../../src/components/Game';
import { GameState } from '../../src/models/GameState';
import { levelConfigs } from '../../src/utils/levels';

describe('Game', () => {
  test('should initialize with the correct board size and mine count', () => {
    const { rows, cols, mineCount } = levelConfigs.easy;
    const game = new Game(rows, cols, mineCount);

    expect(game.getRows()).toBe(rows);
    expect(game.getCols()).toBe(cols);
    expect(game.getRemainingMineCount()).toBe(mineCount);
  });

  test('should reveal a cell and update the game state correctly', () => {
    const { rows, cols, mineCount } = levelConfigs.easy;
    const game = new Game(rows, cols, mineCount);

    game.revealCell(0, 0);
    expect(game.getGameState()).toBe(GameState.InProgress);

  });

  test('should flag and unflag a cell correctly', () => {
    const { rows, cols, mineCount } = levelConfigs.easy;
    const game = new Game(rows, cols, mineCount);
  
    game.flagCell(0, 0);
    expect(game.getRemainingMineCount()).toBe(mineCount - 1);
  
    game.flagCell(0, 0);
    expect(game.getRemainingMineCount()).toBe(mineCount);
  });
});