# Minesweeper Game

This is a Minesweeper game application built with TypeScript. It allows users to play the classic Minesweeper game with different difficulty levels.

## Prerequisites

- Node.js (version 14.x or later)
- npm (version 6.x or later)

## Installation

1. Clone the repository:
https://gitlab.com/dilshodbekisoqjonov/final_project_typescript_dilshodbek_isokjonov


2. Navigate to the project directory:
cd minesweeper-game

3. Install the dependencies:
npm install


## Running the Project

### Development Mode

To run the project in development mode, use the following command:
npm start


This will start the webpack development server and automatically open your default browser with the Minesweeper game. Any changes you make to the source files will be automatically reflected in the browser.

### Production Build

To create a production build, use the following command:
npm run build


This will transpile and bundle the TypeScript code into a single JavaScript file (`bundle.js`) in the `dist` folder.

After the build is complete, you can open the `src/index.html` file in your browser to run the Minesweeper game.

## Running in the Browser

To run the Minesweeper game directly in the browser, follow these steps:

1. Build the project for production:
npm run build


2. Open the `src/index.html` file in your web browser.

You should now see the Minesweeper game running in your browser.

## Notes

- This project was developed using Node.js version 14.x and npm version 6.x.
- The project uses webpack for bundling and transpiling the TypeScript code.
- The source code is located in the `src` folder, and the transpiled code is output to the `dist` folder.

If you encounter any issues or have questions, please feel free to open an issue or submit a pull request

