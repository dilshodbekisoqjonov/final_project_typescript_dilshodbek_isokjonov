import { GameBoard } from '../models/GameBoard';
import { GameState } from '../models/GameState';

export class Game {
  private board: GameBoard;
  private state: GameState;

  constructor(rows: number, cols: number, mineCount: number) {
    this.board = new GameBoard(rows, cols, mineCount);
    this.state = GameState.InProgress;
  }

  public revealCell(row: number, col: number): void {
    if (this.state !== GameState.InProgress) {
      return; // Game is not in progress, do nothing
    }

    const cell = this.board.getCell(row, col);

    if (cell.isMine) {
      this.state = GameState.Lost;
      this.board.revealAllMines();
    } else {
      this.board.revealCell(row, col);
      if (this.board.allNonMineCellsRevealed()) {
        this.state = GameState.Won;
      }
    }
  }

  public flagCell(row: number, col: number): void {
    if (this.state !== GameState.InProgress) {
      return; // Game is not in progress, do nothing
    }

    this.board.flagCell(row, col);
  }

  public getGameState(): GameState {
    return this.state;
  }

  public getRows(): number {
    return this.board.rows;
  }
  
  public getCols(): number {
    return this.board.cols;
  }

  public getRemainingMineCount(): number {
    let remainingMines = this.board.mineCount;
  
    for (let row = 0; row < this.board.rows; row++) {
      for (let col = 0; col < this.board.cols; col++) {
        const cell = this.board.getCell(row, col);
        if (cell.isFlagged && !cell.isMine) {
          remainingMines++;
        } else if (!cell.isFlagged && cell.isMine) {
          remainingMines--;
        }
      }
    }
  
    return remainingMines;
  }

  public isMine(row: number, col: number): boolean {
    const cell = this.board.getCell(row, col);
    return cell.isMine;
  }

  // Add other methods as needed
}