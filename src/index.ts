import { Game } from './components/Game';
import { levelConfigs } from './utils/levels';
import { GameState } from './models/GameState';

const gameContainer = document.getElementById('game-container')!;
const gameBoard = document.getElementById('game-board')!;
const gameStateElement = document.getElementById('game-state')!;
const mineCountElement = document.getElementById('mine-count')!;
const resetButton = document.getElementById('reset-button')!;
const difficultySelect = document.getElementById('difficulty-select')!;

let currentGame: Game;

function initializeGame(level: string) {
  const { rows, cols, mineCount } = levelConfigs[level];
  currentGame = new Game(rows, cols, mineCount);
  renderGameBoard();
  updateGameState();
  updateMineCount();
}


const handleCellClick = (event: MouseEvent) => {
  const cell = event.target as HTMLDivElement;
  const row = parseInt(cell.dataset.row!);
  const col = parseInt(cell.dataset.col!);

  currentGame.revealCell(row, col);
  renderGameBoard();
  updateGameState();
};

const handleCellRightClick = (event: MouseEvent) => {
  event.preventDefault();
  const cell = event.target as HTMLDivElement;
  const row = parseInt(cell.dataset.row!);
  const col = parseInt(cell.dataset.col!);

  currentGame.flagCell(row, col);
  renderGameBoard();
  updateMineCount();
};



function renderGameBoard() {
  gameBoard.innerHTML = '';

  for (let row = 0; row < currentGame.getRows(); row++) {
    for (let col = 0; col < currentGame.getCols(); col++) {
      const cell = document.createElement('div');
      cell.classList.add('cell');
      cell.dataset.row = row.toString();
      cell.dataset.col = col.toString();
      cell.addEventListener('click', handleCellClick);
      cell.addEventListener('mousedown', handleCellRightClick);
      gameBoard.appendChild(cell);
    }
  }
}
function disableGameBoard() {
  const cells = gameBoard.querySelectorAll('.cell');
  cells.forEach((cell) => {
    cell.removeEventListener('click', handleCellClick as EventListener);
    cell.removeEventListener('contextmenu', handleCellRightClick as EventListener);
  });
}

function updateGameState() {
  const gameState = currentGame.getGameState();
  switch (gameState) {
    case GameState.InProgress:
      gameStateElement.textContent = 'In Progress';
      break;
    case GameState.Won:
      gameStateElement.textContent = 'You Won!';
      disableGameBoard();
      break;
    case GameState.Lost:
      gameStateElement.textContent = 'Game Over!';
      disableGameBoard();
      revealAllMines();
      break;
  }
}

function updateMineCount() {
  const remainingMines = currentGame.getRemainingMineCount();
  mineCountElement.textContent = `Mines: ${remainingMines}`;
}

function revealAllMines() {
  const cells = gameBoard.querySelectorAll('.cell');
  cells.forEach((cell) => {
    const row = parseInt((cell as HTMLDivElement).dataset.row!);
    const col = parseInt((cell as HTMLDivElement).dataset.col!);
    if (currentGame.isMine(row, col)) {
      cell.classList.add('mine');
    }
  });
}


// Event listeners
// Event listeners
resetButton.addEventListener('click', () => initializeGame((difficultySelect as HTMLSelectElement).value));
difficultySelect.addEventListener('change', () => initializeGame((difficultySelect as HTMLSelectElement).value));

// Initialize the game with the default difficulty level
initializeGame('easy');