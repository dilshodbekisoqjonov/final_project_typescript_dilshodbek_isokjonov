export interface LevelConfig {
  rows: number;
  cols: number;
  mineCount: number;
}

export const levelConfigs: { [key: string]: LevelConfig } = {
  easy: {
    rows: 10,
    cols: 10,
    mineCount: 5,
  },
  medium: {
    rows: 20,
    cols: 20,
    mineCount: 10,
  },
  // Add more difficulty levels as needed
};