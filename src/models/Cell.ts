export class Cell {
  isMine: boolean;
  isRevealed: boolean;
  isFlagged: boolean;
  adjacentMineCount: number;

  constructor(isMine: boolean) {
    this.isMine = isMine;
    this.isRevealed = false;
    this.isFlagged = false;
    this.adjacentMineCount = 0;
  }
}