import { Cell } from "./Cell";

export class GameBoard {
  private board: Cell[][];

  constructor(rows: number, cols: number, mineCount: number) {
  
    this.board = this.initializeBoard();
  }

  public get rows(): number {
    return this.rows;
  }
  
  public get cols(): number {
    return this.cols;
  }

  public get mineCount(): number {
    return this.mineCount;
  }

  private generateMinePositions(): Set<string> {
    const minePositions = new Set<string>();
  
    while (minePositions.size < this.mineCount) {
      const row = Math.floor(Math.random() * this.rows);
      const col = Math.floor(Math.random() * this.cols);
      const position = `${row},${col}`;
  
      if (!minePositions.has(position)) {
        minePositions.add(position);
      }
    }
  
    return minePositions;
  }

  private getAdjacentMineCount(row: number, col: number): number {
    let count = 0;
  
    // Check all adjacent cells
    for (let r = Math.max(row - 1, 0); r <= Math.min(row + 1, this.rows - 1); r++) {
      for (let c = Math.max(col - 1, 0); c <= Math.min(col + 1, this.cols - 1); c++) {
        if (r !== row || c !== col) {
          if (this.board[r][c].isMine) {
            count++;
          }
        }
      }
    }
  
    return count;
  }

  private initializeBoard(): Cell[][] {
    const board: Cell[][] = [];

    
    // Generate mines randomly
    const minePositions = this.generateMinePositions();

    for (let row = 0; row < this.rows; row++) {
      board[row] = [];
      for (let col = 0; col < this.cols; col++) {
        const isMine = minePositions.has(`${row},${col}`);
        board[row][col] = new Cell(isMine);
      }
    }

    // Calculate adjacent mine counts
    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        if (!board[row][col].isMine) {
          board[row][col].adjacentMineCount = this.getAdjacentMineCount(
            row,
            col
          );
        }
      }
    }

    return board;
  }

  private revealAdjacentCells(row: number, col: number): void {
    // Check all adjacent cells
    for (let r = Math.max(row - 1, 0); r <= Math.min(row + 1, this.rows - 1); r++) {
      for (let c = Math.max(col - 1, 0); c <= Math.min(col + 1, this.cols - 1); c++) {
        if (r !== row || c !== col) {
          const cell = this.board[r][c];
  
          if (!cell.isRevealed && !cell.isFlagged) {
            this.revealCell(r, c);
          }
        }
      }
    }
  }
  public revealCell(row: number, col: number): void {
    const cell = this.board[row][col];

    if (!cell.isRevealed && !cell.isFlagged) {
      cell.isRevealed = true;

      if (cell.adjacentMineCount === 0) {
        // Reveal adjacent cells recursively
        this.revealAdjacentCells(row, col);
      }
    }
  }

  public flagCell(row: number, col: number): void {
    const cell = this.board[row][col];

    if (!cell.isRevealed) {
      cell.isFlagged = !cell.isFlagged;
    }
  }
  public getCell(row: number, col: number): Cell {
    if (row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
      throw new Error("Invalid row or column");
    }
    return this.board[row][col];
  }

  public revealAllMines(): void {
    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        const cell = this.board[row][col];
        if (cell.isMine) {
          cell.isRevealed = true;
        }
      }
    }
  }
  
  public allNonMineCellsRevealed(): boolean {
    for (let row = 0; row < this.rows; row++) {
      for (let col = 0; col < this.cols; col++) {
        const cell = this.board[row][col];
        if (!cell.isMine && !cell.isRevealed) {
          return false;
        }
      }
    }
    return true;
  }
}
